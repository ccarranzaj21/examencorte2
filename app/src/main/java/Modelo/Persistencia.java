package Modelo;

import com.example.examencorte2.Ventas;
public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertVenta(Ventas venta);

}