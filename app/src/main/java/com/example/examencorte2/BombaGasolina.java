package com.example.examencorte2;

public class BombaGasolina {
    private int numeroBomba;
    private String tipoGasolina;
    private double precio;
    private int capacidad;
    private int contadorLitros;

    public int getNumeroBomba() {
        return numeroBomba;
    }

    public void setNumeroBomba(int numeroBomba) {
        this.numeroBomba = numeroBomba;
    }

    public String getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(String tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public int getContadorLitros() {
        return contadorLitros;
    }

    public void setContadorLitros(int contadorLitros) {
        this.contadorLitros = contadorLitros;
    }
}
