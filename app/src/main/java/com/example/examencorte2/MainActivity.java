package com.example.examencorte2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {
    private EditText editTextNumBomba, editTextPrecio, editTextCapacidad, editTextCantidadLitros;
    private RadioButton radioButtonRegular, radioButtonExtra;
    private Button buttonIniciarBomba, buttonHacerVenta, buttonRegistrarVenta, buttonConsultarRegistro, buttonSalir, buttonLimpiar;

    private BombaGasolina bombaGasolina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarComponentes();

        buttonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSalir();
            }
        });
    }
    private void inicializarComponentes() {
        buttonIniciarBomba = findViewById(R.id.buttonIniciarBomba);
        buttonHacerVenta = findViewById(R.id.buttonHacerVenta);
        buttonRegistrarVenta = findViewById(R.id.buttonRegistrarVenta);
        buttonConsultarRegistro = findViewById(R.id.buttonConsultarRegistro);
        buttonSalir = findViewById(R.id.btnSalir);
        buttonLimpiar = findViewById(R.id.btnLimpiar);
        editTextNumBomba = findViewById(R.id.editTextNumBomba);
        editTextCantidadLitros = findViewById(R.id.editTextCantidadLitros);
        editTextPrecio = findViewById(R.id.editTextPrecio);
        editTextCapacidad = findViewById(R.id.editTextCapacidad);
        radioButtonExtra = findViewById(R.id.radioButtonExtra);
        radioButtonRegular = findViewById(R.id.radioButtonRegular);


    }


    private void btnSalir(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Confirmación");
        builder.setMessage("¿Estás seguro de que quieres cerrar la aplicación?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }


}